清空git远程的密码重新输入
git config --system --unset credential.helper

存储git远端密码，避免每次推送远端不断输入密码
git config --system credential.helper store

设置单个项目的提交人员信息
git config --local user.name 'zhangsan'
git config --local user.email  'ddd@qq.com'


删除远程分支

git push origin :dev
git push origin --delete dev
git push origin -d dev